<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => false,
    'module'      => 'Module',
    'require'     => ['skimia.modules']
];