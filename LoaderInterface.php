<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/11/2014
 * Time: 19:26
 */

namespace Skimia\Modules;


interface LoaderInterface {

    /**
     * Charge un module
     *
     * @param  string  $author
     * @param  string  $module
     * @param  bool  $force force le chargement même s'il est desactivé
     * @return array
     */
    public function load($author, $module, $force = false);

    /**
     * Ajoute un repertoire de recherche
     *
     * @param  string  $directory
     * @return void
     */
    public function addDirectory($directory);

    /**
     * Charge tous les Modules
     * @param bool $force force le chargement même s'il est désactivé
     * @return void
     */
    public function loadAll($force = false);
}