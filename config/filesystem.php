<?php

return [
    'module' => [
        'file' => [
            'info' =>'module.info.php'
        ]
    ],
    'modules' => [
        'dir' => [
            'path' => base_path().'/modules'
        ]
    ],
    'core' => [
        'dir' => [
            'path' => base_path().'/core'
        ]
    ],
    'autoload'=>[
        'dir'=>'src',
    ]
];