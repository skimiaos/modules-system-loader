<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/11/2014
 * Time: 20:02
 */

namespace Skimia\Modules;

use File;
use Event;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\Finder;
use ReflectionClass;
use Collection;
class ModuleBase extends ServiceProvider{

    protected $info = array();

    public final function __construct($app, $info)
    {
        $this->app = $app;

        $this->info = $info;
    }

    public function package($package, $namespace = null, $path = null)
    {
        $namespace = $this->getPackageNamespace($package, $namespace);

        // In this method we will register the configuration package for the package
        // so that the configuration options cleanly cascade into the application
        // folder to make the developers lives much easier in maintaining them.
        $path = $path ?: $this->guessPackagePath();


        $config = $path.'/resources/config';
        if ($this->app['files']->isDirectory($config))
        {
            $this->app['config']->package($package, $config, $namespace);
        }

        // Next we will check for any "language" components. If language files exist
        // we will register them with this given package's namespace so that they
        // may be accessed using the translation facilities of the application.
        $lang = $path.'/resources/lang';

        if ($this->app['files']->isDirectory($lang))
        {
            $this->app['translator']->addNamespace($namespace, $lang);
        }

        // Next, we will see if the application view folder contains a folder for the
        // package and namespace. If it does, we'll give that folder precedence on
        // the loader list for the views so the package views can be overridden.
        $appView = $this->getAppViewPath($package);

        if ($this->app['files']->isDirectory($appView))
        {
            $this->app['view']->addNamespace($namespace, $appView);
        }

        // Finally we will register the view namespace so that we can access each of
        // the views available in this package. We use a standard convention when
        // registering the paths to every package's views and other components.
        $view = $path.'/resources/views';
        if ($this->app['files']->isDirectory($view))
        {
            $this->app['view']->addNamespace($namespace, $view);
        }
    }

    /**
     * Initilise le module
     * * Fais appel a la fonction de laravel package()
     * * Ajoute la config ( '/resources/config/' )
     * * Inclue les registers ( '/register.php' | '/register/' )
     * * Inclue les filters ( '/resources/filters.php' | '/resources/filters/' )
     * * Inclue le routing ( '/resources/routing.php' | '/resources/routes/' )
     * @see Illuminate\Support\ServiceProvider::package
     */
    public final function init(){
        \Assets::getModulePublicAssetsWebDir($this->getCanonical());
        $this->package($this->getCanonical(),$this->getCanonical('.'));

        $path = $this->guessPackagePath();

        $config = $path.'/resources/config';

        if ($this->app['files']->isDirectory($config))
        {
            Event::fire('skimia.modules::module_base.init.config',[$this,$config]);
        }

        if(File::exists($path.'/register.php'))
            require $path.'/register.php';

        if(File::exists($path.'/register')){
            $files = $this->allFilesSorted($path.'/register');
            foreach($files as $file){
                $pinfo = pathinfo($file);
                if(isset($pinfo['extension']) && $pinfo['extension'] == 'php')
                    require_once $file;
            }
        }

        //chargement du fichier de routing a la racine du module
        if(File::exists($path.'/resources/filters.php'))
            require $path.'/resources/filters.php';

        if(File::exists($path.'/resources/filters')){
            $files = $this->allFilesSorted($path.'/resources/filters');
            foreach($files as $file){
                if(pathinfo($file)['extension'] == 'php')
                    require_once $file;
            }
        }


        if(File::exists($path.'/resources/routing.php'))
            require $path.'/resources/routing.php';

        if(File::exists($path.'/resources/routes.php'))
            require $path.'/resources/routes.php';

        if(File::exists($path.'/resources/routes')){
            $files = $this->allFilesSorted($path.'/resources/routes');
            foreach($files as $file){
                if(pathinfo($file)['extension'] == 'php')
                    require_once $file;
            }
        }

    }

    protected function allFilesSorted($directory){
        return iterator_to_array(Finder::create()->files()->in($directory)->sortByName(), false);
    }

    /**
     * Envoie l'event de boot du module
     * EVENT : "skimia.modules.module.boot"
     */
    public final function boot(){
        $this->preBoot();

        Event::fire('skimia.modules.module.boot', array($this));
        $this->postBoot();
    }

    /**
     * Lancé avant le lancement de l'évent
     * placer ici du code
     */
    public function preBoot(){

    }
    /**
     * Lancé apres le lancement de l'évent
     * placer ici du code
     */
    public function postBoot(){

    }

    /**
     * Ajoute a l'alias loader de laravel les facades du module
     * @return array en clef l'alias a généner et en valeur le chemin complet de la classe
     */
    public function getAliases(){
        return [];
    }

    /**
     * Retourne les infos du module
     * @todo voir car la fonction ne retourne un array vide car le constructeur reçoit rien
     * @return Collection
     */
    public function getInfo(){
        return $this->info;
    }

    /**
     * Register the service provider.
     *
     * EVENT : "skimia.modules.module.register"
     * @return void
     */
    public function register()
    {
        Event::fire('skimia.modules.module.register', array($this));

    }


    /**
     * Retourne le nom cannonique du module
     * le plus souvent organisation/module
     *     On peut utiliser aussi organisation.module mais c'est deconseillé
     * @param bool $char
     * @return mixed
     */
    protected function getCanonical($char = false){
        if($char)
            return str_replace('/', $char, $this->info['canonical']);
        else
            return $this->info['canonical'];
    }

    /**
     * Retourne le chemin du module
     * Pour un usage par laravel
     * @return string
     */
    public function guessPackagePath()
    {
        $path = (new ReflectionClass($this))->getFileName();
        return realpath(dirname(dirname($path)));
    }

    /**
     * Lancé avant le register
     *  au chargement du module
     * faire ici des modifications sur du core
     * * placer ici du code
     */
    public function beforeRegisterModule(){

    }
}
