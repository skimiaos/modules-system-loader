<?php

namespace{
    use Skimia\Modules\Modules;

    function module_path($module_canonical,$path = false){

        $mod_path = Modules::getModuleInfo($module_canonical)['path'];

        if(!ends_with($mod_path,['/','\\']))
            $mod_path .='/';

        if($path !== false){
            if(starts_with($path,['/','\\']))
                $path = substr($path,1);
            return $mod_path.$path;
        }else
            return $mod_path;

    }

    function module_resources($module_canonical,$path = false){

        $path = $path ? (\Str::startsWith($path,['/','\\']) ? '':'/').$path:'';
        return module_path($module_canonical, '/resources'.$path);
    }

    function module_assets($module_canonical,$path =false){

        $path = $path ? (\Str::startsWith($path,['/','\\']) ? '':'/').$path:'';
        return module_resources($module_canonical,'/assets'.$path);
    }

    function module_libs($module_canonical,$path =false){

        $path = $path ? (\Str::startsWith($path,['/','\\']) ? '':'/').$path:'';
        return module_resources($module_canonical,'/libs'.$path);
    }

    function module_lib($module_canonical,$lib,$path = false){
        $path = $path ? (\Str::startsWith($path,['/','\\']) ? '':'/').$path:'';
        return module_libs($module_canonical, (\Str::startsWith($lib,['/','\\']) ? '':'/').$lib.$path);
    }
}