<?php

namespace Skimia\Modules;

use Illuminate\Support\ServiceProvider;
use ReflectionClass;
use Config;
use Event;
use File;
use Artisan;
use Skimia\Modules\Commands\AclInstallCommand;
use Skimia\Modules\Commands\ConfigPublishCommand;
use Skimia\Modules\Commands\ModuleMigrateCommand;
use Skimia\Modules\Commands\InstallCommand;

class ModulesServiceProvider extends ServiceProvider{

    public function boot(){
        $this->package('skimia/modules','skimia.modules');

        $fs =File::allFiles(__DIR__.'/functions');
        foreach ($fs as $f) {
            require_once $f;
        }

        Modules::setLoader(new DirectoryLoader(
            $this->app['files'],
            Config::get('skimia.modules::filesystem.modules.dir.path'),
            Config::get('skimia.modules::filesystem.core.dir.path')
        ));
        Modules::registerAutoload(Config::get('skimia.modules::filesystem.autoload.dir'));


        measure('Chargement des Modules', function() {
            global $argv;
            if( !(php_sapi_name() == 'cli' && count($argv) == 2 && $argv[0] == "artisan" && $argv[1] == "dump-autoload")){
                Modules::init();
            }
        });



        $this->app->bindShared('command.skimia.modules.config.publish', function($app)
        {
            return new ConfigPublishCommand($app['config.publisher']);
        });

        $this->app->bindShared('command.skimia.modules.migrate', function($app)
        {
            return new ModuleMigrateCommand();
        });

        $this->app->bindShared('command.skimia.modules.install', function($app)
        {
            return new InstallCommand();
        });

        $this->app->bindShared('command.skimia.acl.install', function($app)
        {
            return new AclInstallCommand();
        });

        $this->commands(
            'command.skimia.modules.config.publish',
            'command.skimia.modules.migrate',
            'command.skimia.modules.install',
            'command.skimia.acl.install'
        );

    }

    public function register(){

    }
    public function guessPackagePath()
    {
        $path = (new ReflectionClass($this))->getFileName();
        return realpath(dirname($path));
    }
}