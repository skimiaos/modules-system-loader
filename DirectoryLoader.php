<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/11/2014
 * Time: 19:30
 */

namespace Skimia\Modules;

use File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Config;
class DirectoryLoader implements LoaderInterface{

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Chemin par defaut des modules
     *
     * @var string
     */
    protected $defaultPath;

    /**
     * Chemin par defaut du core
     *
     * @var string
     */
    protected $corePath;

    /**
     * All of the namespace hints.
     *
     * @var array
     */
    protected $directories = array();

    protected $defaultInfo = array();

    public function __construct(Filesystem $files, $defaultPath, $corePath){
        $this->files = $files;
        $this->defaultPath = $defaultPath;
        $this->corePath = $corePath;
        $this->defaultInfo = require 'default.info.php';
    }

    /**
     * Charge un module
     *
     * @param  string $author
     * @param  string $module
     * @param  bool $force force le chargement même s'il est desactivé
     * @return array
     */
    public function load($author, $module, $force = false)
    {
        // TODO: Implement load() method.
    }

    /**
     * Ajoute un repertoire de recherche
     *
     * @param  string $directory
     * @return void
     */
    public function addDirectory($directory)
    {
        $this->directories[] = $directory;
    }

    /**
     * Charge tous les Modules
     * @param bool $force force le chargement même s'il est désactivé
     * @return void
     */
    public function loadAll($force = false)
    {
        return $this->findAvailablesModules($force);

    }


    protected function findAvailablesModules($force = false){//$force unused

        $modules = array();
        $dirs = $this->directories;
        $dirs[] = $this->defaultPath;

        $core_dirs = File::directories($this->corePath);
        sort ($core_dirs);

        foreach($core_dirs as $core_path){
            if(File::exists($core_path.'/'.Config::get('skimia.modules::filesystem.module.file.info'))){
                $module = lcfirst(str_replace($this->corePath.DIRECTORY_SEPARATOR,'',$core_path));
                $modules[] = $this->loadInfo($core_path, 'skimia', $module);
            }
        }

        foreach($dirs as $directory){
            $authors_directories = File::directories($directory);

            foreach($authors_directories as $author_dir){
                $modules_dirs = File::directories($author_dir);
                $author = lcfirst(str_replace($directory.DIRECTORY_SEPARATOR,'',$author_dir));
                foreach($modules_dirs as $module_path){
                    if(File::exists($module_path.'/'.Config::get('skimia.modules::filesystem.module.file.info'))){
                        $module = lcfirst(str_replace($author_dir.DIRECTORY_SEPARATOR,'',$module_path));
                        $modules[] = $this->loadInfo($module_path, $author, $module);
                    }
                }

            }

        }



        return $modules;
    }

    protected function loadInfo($module_path, $author, $module){

        $module_info = require $module_path.'/'.Config::get('skimia.modules::filesystem.module.file.info');
        $module_info['path']= $module_path;
        $module_info['canonical'] = $author.'/'.$module;
        return new Collection(array_merge($this->defaultInfo,$module_info));
    }
}