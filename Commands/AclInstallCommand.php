<?php

namespace Skimia\Modules\Commands;

use Illuminate\Console\Command;
use Skimia\Auth\Managers\AclManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Event;

class AclInstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'os:acl:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Installation des droits.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$manager  = new AclManager(\App::make('acl'));

		Event::fire('artisan.os.install.acl',[$manager]);

		$manager->save();


	}

}
