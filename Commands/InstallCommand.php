<?php

namespace Skimia\Modules\Commands;

use Illuminate\Console\Command;
use Skimia\Auth\Facades\AclActions;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Event;

class InstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'os:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Installation de l\'os.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        if(\Schema::hasTable('migrations')){
            Event::fire('artisan.os.install.reset.before');
            $this->call('migrate:reset',['--force'=>true]);
            Event::fire('artisan.os.install.reset.after');
        }



        Event::fire('artisan.os.install.before');

        Event::fire('artisan.os.install.migrate',[$this]);
        $this->call('modules:migrate');

        Event::fire('artisan.os.install.seed',[$this]);

        Event::fire('artisan.os.install.finish',[$this]);

        Event::fire('artisan.os.install.after');


	}

}
