<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 13/11/2014
 * Time: 22:59
 */
namespace Skimia\Modules\Commands;
use Illuminate\Foundation\Console\ConfigPublishCommand as ConfigPublishCommandBase;
use Illuminate\Console\ConfirmableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ConfigPublishCommand extends ConfigPublishCommandBase{

    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'modules:config:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Publish a module's configuration to the application";

    public function fire(){
        $this->input->setArgument('package',str_replace('.','/',$this->input->getArgument('package')));
        parent::fire();
    }
    protected function getPath()
    {
        $path = $this->input->getOption('path');

        if ( ! is_null($path))
        {
            return $this->laravel['path.base'].'/'.$path;
        }else{

            $module = $this->input->getArgument('package');

            return \Config::get('skimia.modules::filesystem.modules.dir.path').'/'.$module.'/config';
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('package', InputArgument::REQUIRED, 'The name of the module being published.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('path', null, InputOption::VALUE_OPTIONAL, 'The path to the configuration files.', null),

            array('force', null, InputOption::VALUE_NONE, 'Force the operation to run when the file already exists.'),
        );
    }
} 