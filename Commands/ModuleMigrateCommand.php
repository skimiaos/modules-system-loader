<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 15/11/2014
 * Time: 10:40
 */

namespace Skimia\Modules\Commands;

use Illuminate\Console\Command;
use Config;
use Illuminate\Console\ConfirmableTrait;
use Skimia\Modules\Modules;

class ModuleMigrateCommand extends Command{


    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'modules:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run Migrations For modules";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        global $app;

        $modules = Modules::getModules();
        foreach($modules as $module){
            $path =  str_replace($app['path.base'].'/','',$module['path']).DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Data'.DIRECTORY_SEPARATOR.'Migrations';
            if(\File::exists($path)){
                $this->call('migrate', array('--force' => true,'--path' =>  $path));
            }

        }

    }
} 