<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/11/2014
 * Time: 19:25
 */


namespace Skimia\Modules {

    use App;
    use DebugBar\DebugBar;
    use Exception;

    class Modules
    {

        static $loader;

        static $modules = array();
        static $autoload = array();
        static $autoload_dir = false;

        /**
         * Indicates if a ClassLoader has been registered.
         *
         * @var bool
         */
        protected static $registered = false;

        public static function setLoader(LoaderInterface $loader)
        {

            self::$loader = $loader;
        }

        public static function init()
        {
            global $app;
            self::$modules = self::$loader->loadAll();

            $module_instances = array();

            foreach (self::$modules as $module) {

                if ($module['namespace']) {
                    self::$autoload[$module['namespace']] = $module['path'];
                }

                if ($module['module']) {
                    $module_class = rtrim($module['namespace'], '\\') . '\\' . $module['module'];
                } else {

                }

                if (class_exists($module_class)) {


                    if (!is_subclass_of($module_class, '\Skimia\Modules\ModuleBase'))
                        throw new Exception('la classe [' . $module_class . '] du module doit etendre de \Skimia\Modules\ModuleBase');

                    $module_instance = new $module_class($app, $module);
                    $module_instances[] = $module_instance;

                    static::setAliases($module_instance->getAliases());

                }
            }

            foreach ($module_instances as $module_instance) {

                if (method_exists($module_instance, 'beforeRegisterModules')) {
                    $static = (new \ReflectionClass(get_class($module_instance)))->getMethod('beforeRegisterModules')->isStatic();
                    if (!$static) {
                        call_user_func([$module_instance, 'beforeRegisterModules']);
                    } else
                        call_user_func([get_class($module_instance), 'beforeRegisterModules']);
                }
            }

            foreach ($module_instances as $module_instance) {
                $module_instance->init();
                App::register($module_instance);
            }


            \Event::listen('artisan.os.install.reset.before',function(){
                \DB::raw('SET FOREIGN_KEY_CHECKS = 0;');
            });
            \Event::listen('artisan.os.install.reset.after',function(){
                \DB::raw('SET FOREIGN_KEY_CHECKS = 1;');
            });
            \Event::listen('artisan.os.install.before',function(){
                \DB::raw('SET FOREIGN_KEY_CHECKS = 0;');
            });
            \Event::listen('artisan.os.install.after',function(){
                \DB::raw('SET FOREIGN_KEY_CHECKS = 1;');
            });
        }

        public static function setAliases($arr){
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();

            foreach($arr as $alias=>$class_name){
                $loader->alias($alias, $class_name);
            }
        }
        public static function registerAutoload($autoload_dir = false)
        {
            if (!static::$registered) {
                static::$autoload_dir = $autoload_dir;
                static::$registered = spl_autoload_register(array('\Skimia\Modules\Modules', 'autoload'));
            }
        }

        public static function getModuleInfo($canonical_name)
        {
            foreach (self::$modules as $module) {
                if ($module['canonical'] == $canonical_name || $canonical_name == str_replace('/','.',$module['canonical'])) {
                    return $module;
                }
            }

            return false;
        }


        public static function hasModule($canonical_name)
        {
            foreach (self::$modules as $module) {
                if ($module['canonical'] == $canonical_name || $canonical_name == str_replace('/','.',$module['canonical'] )) {
                    return true;
                }
            }

            return false;
        }

        public static function moduleState($canonical_name){
            if(!static::hasModule($canonical_name))
                return -1;
            return 1;
        }

        public static function getModules()
        {
            return self::$modules;
        }

        /**
         * Get the normal file name for a class.
         *
         * @param  string $class
         * @return string
         */
        public static function normalizeClass($class)
        {
            if ($class[0] == '\\') $class = substr($class, 1);
            foreach (self::$autoload as $autoload => $dir) {
                if (starts_with($class, $autoload))
                    return $dir . ( static::$autoload_dir ? '/' . static::$autoload_dir : '' ) . '/' . str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, str_replace($autoload, '', $class)) . '.php';
            }

            return str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, $class) . '.php';
        }

        /**
         * Load the given class file.
         *
         * @param  string $class
         * @return bool
         */
        public static function autoload($class)
        {
            $class = self::normalizeClass($class);

            if ($class) {
                if (\File::exists($class)) {
                    require_once $class;
                    return true;
                }

            }

            return false;
        }
    }
}